# RACER

RACER is a racing game where you are the red car and you have to avoid other cars because you go crazy fast.

## Screenshots

![Menu](https://i.imgur.com/6mPDGEq.png)
![Game](https://i.imgur.com/rk1oJ5e.png)
![Crash](https://i.imgur.com/OEcEQal.png)

## Requirements

RACER requires [Python 3.7](https://www.python.org/) and [PyGame](https://www.pygame.org/) module to run.

Install the dependencies and start the game.

```sh
$ python -m pip --upgrade pip
$ python -m pip install pygame
$ python RACER.py
```

## Controls

RACER is using different keys on the keyboard to play. These keys are listed below.

| KEY | KEYCODE | FUNCTION |
| ------ | ------ | ------|
| UP | K_UP | Make the car accelerate |
| DOWN | K_DOWN | Make the car slow down  |
| LEFT | K_LEFT | Turn left |
| RIGHT | K_RIGHT | Turn right |
| ENTER | K_RETURN | Exit in menu |
| SPACE | K_SPACE | Start in menu |
| C | K_c | Crash the car and end the game |
| ? | ? | ? Unknown ? |

License
----

MIT (Check "license.txt" file for more informations)
